#!/bin/sh

if [[ "$1" == "34d45bd7-4d76-4e12-bfc8-b7feac33779a" && "$2" == "device_mounted" ]];
then
    echo ""
else
    exit 0
fi

LAST_BACKUP_FILE="/home/max/.dwm/lastbackup.txt"
LAST_STAMPED_BACKUP=`cat "/home/max/.dwm/laststampedbackup.txt"`
LAST_BACKUP=`cat $LAST_BACKUP_FILE`
CURRENT_DATE=`date +%s`
SECOND_DIFF=`echo "$(($CURRENT_DATE-$LAST_BACKUP))"`
STAMPED_SECOND_DIFF=`echo "$(($CURRENT_DATE-$LAST_STAMPED_BACKUP))"`
DIFF=$(echo "scale=2; $SECOND_DIFF/3600" | bc)
STAMPED_DIFF=$(echo "scale=2; $STAMPED_SECOND_DIFF/3600" | bc)

DIRS=("documents" "sync")

notify-send "Backing up drive..."

if [ 1 -eq "$(echo "${STAMPED_DIFF} > 168" | bc)" ];
then
    DATE=$(date +%H%M_%d-%m-%y)
    notify-send "Backup" "Creating new tarball as one week has passed..."
    for DIR in "${DIRS[@]}";
    do
        rsync -rh /home/max/$DIR /run/media/max/BACKUP/backups/$DIR/$DIR.$DATE
        tar cvzf /run/media/max/BACKUP/backups/$DIR/$DIR.$DATE.tar.gz /run/media/max/BACKUP/backups/$DIR/$DIR.$DATE
        rm -rf /run/media/max/BACKUP/backups/$DIR/$DIR.$DATE
    done
    echo $CURRENT_DATE > $LAST_STAMPED_BACKUP
else
    for DIR in "${DIRS[@]}";
    do
        rsync -rh /home/max/$DIR/ /run/media/max/BACKUP/backups/$DIR/$DIR
    done
fi

echo $CURRENT_DATE > $LAST_BACKUP_FILE
notify-send "Drive successfully backed up!" $DIFF" hours since last successful backup."
udisksctl unmount -b /dev/sda1
