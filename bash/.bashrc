# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias code='cd $HOME/workspace/programming/'
alias e='emacsclient -c'
alias sail='sh $([ -f sail ] && echo sail || echo vendor/bin/sail)'
alias doom='$HOME/.config/emacs/bin/doom'

export GOPATH="/home/"$USER"/workspace/go"
export GOSRC=$GOPATH/src/
export GOBIN=$GOPATH/bin/

export PATH=$PATH:$GOBIN:"/home/max/.local/bin":"/home/max/.config/composer/vendor/bin/"

export LFS=/mnt/lfs

function emacsman() {
    e -e "(let ((Man-notify-method 'bully)) (man \"$1\"))"
}

#export PS1="\[\033[38;5;12m\][\[$(tput sgr0)\]\[\033[38;5;10m\]\u\[$(tput sgr0)\]\[\033[38;5;12m\]@\[$(tput sgr0)\]\[\033[38;5;7m\]\h\[$(tput sgr0)\]\[\033[38;5;12m\]]\[$(tput sgr0)\]\[\033[38;5;15m\]: \[$(tput sgr0)\]\[\033[38;5;7m\]\w\[$(tput sgr0)\]\[\033[38;5;12m\]>\[$(tput sgr0)\]\[\033[38;5;10m\]\\$\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"

[ -e "/etc/DIR_COLORS" ] && DIR_COLORS="/etc/DIR_COLORS"
[ -e "$HOME/.dircolors" ] && DIR_COLORS="$HOME/.dircolors"
[ -e "$DIR_COLORS" ] || DIR_COLORS=""
eval "$(dircolors -b $DIR_COLORS)"

# Run twolfson/sexy-bash-prompt
. ~/.bash_prompt

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"                   # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" # This loads nvm bash_completion

PATH="/home/max/perl5/bin${PATH:+:${PATH}}"
export PATH
PERL5LIB="/home/max/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"
export PERL5LIB
PERL_LOCAL_LIB_ROOT="/home/max/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"
export PERL_LOCAL_LIB_ROOT
PERL_MB_OPT="--install_base \"/home/max/perl5\""
export PERL_MB_OPT
PERL_MM_OPT="INSTALL_BASE=/home/max/perl5"
export PERL_MM_OPT

if [ -n $DISPLAY ]; then
    setxkbmap gb -variant colemak
fi
[ -f /opt/miniconda3/etc/profile.d/conda.sh ] && source /opt/miniconda3/etc/profile.d/conda.sh

# Added by LM Studio CLI (lms)
export PATH="$PATH:/home/max/.cache/lm-studio/bin"
