#!/usr/bin/sh

TEMP_FILE=`mktemp --suffix .backup`
touch $TEMP_FILE

DATE=$(date +%Y-%m-%d-%H%M%S)

BACKUP_DIR="/mnt/data/backups"
SOURCE="$HOME/workspace"

tar -cvzpf $BACKUP_DIR/mybackup-$DATE.tar.gz $SOURCE --exclude $SOURCE/**/node_modules

rm $TEMP_FILE

echo "Workspace tar backup successful"
